import React, { useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Chat from './containers/Chat';
import Login from './containers/Login';
import Settings from './containers/Settings';
import Header from './components/header/Header';
import { getCurrentUser } from './utils/localStorage';

function App() {

  const [ loggedIn, setLoggedIn ] = useState(localStorage.getItem('ra_session') !== null);

  const changeLoggedIn = state => {
    setLoggedIn(state);
  };

  return (
    <Router>
      <div className="container">
        <Header loggedIn={ loggedIn } changeLoggedIn={ changeLoggedIn } />
        <Switch>
          <Route path="/login" render={(props) => (
            loggedIn === false
              ? <Login {...props} changeLoggedIn={ changeLoggedIn } />
              : <Redirect to='/' /> 
          )} />
          <Route path="/settings" render={(props) => (
            loggedIn === true
              ? <Settings {...props} user={getCurrentUser()} />
              : <Redirect to='/login' /> 
          )} />
          <Route path="/" render={(props) => (
            loggedIn === true
              ? <Chat {...props} />
              : <Redirect to='/login' /> 
          )} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
