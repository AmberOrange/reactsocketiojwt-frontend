import React from "react";
import { Link } from "react-router-dom";
import { getCurrentUser } from "../../utils/localStorage";

const Header = (props) => {

  const logout = () => {
    localStorage.clear();
    props.changeLoggedIn(false);
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link className="navbar-brand" to="/">
        Chatroom
      </Link>

      <ul className="navbar-nav mr-auto">
        <li className="nav-item active">
          <Link className="nav-link" to="/">
            Chat
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/settings">
            Settings
          </Link>
        </li>
      </ul>
      <span className="navbar-text">
        { !props.loggedIn && <Link to="/login">Login</Link> }
        { props.loggedIn && <span>{ getCurrentUser().name } <button type="button" onClick={logout} className="btn btn-primary p-1">Logout</button> </span> }
      </span>
    </nav>
  );
};

export default Header;
