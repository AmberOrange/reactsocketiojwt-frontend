import React from "react";

const ChatList = (props) => {
  return (
    <div>
      <h3>These people are active:</h3>
      {props.users.map((item, index) => (
        <div className="row" key={index}>
          <img
            src={item.avatar}
            width="40px"
            alt="John"
          />
          {item.name}
        </div>
      ))}
    </div>
  );
};

export default ChatList;
