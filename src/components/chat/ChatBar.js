import React, { useState } from "react";

const ChatBar = (props) => {
  const [message, setMessage] = useState("");

  const onMessageChange = (ev) => setMessage(ev.target.value);

  const onFormSubmit = (e) => {
    e.preventDefault();
    props.sendMessage(message);
    setMessage("");
  }

  return (
    <div>
      <form className="input-group" onSubmit={onFormSubmit}>
        <input
          type="text"
          placeholder="Enter text..."
          value={message}
          onChange={onMessageChange}
          autoFocus
        />
        <button type="submit">
          Send
        </button>
      </form>
    </div>
  );
};

export default ChatBar;
