import React from "react";

const ChatBox = (props) => {
  return (
    <div>
      <h2>This is the chatbox</h2>
      <div>
        {props.messages.map((item, index) => (
          <div className="row" key={index}>
            <div className="col-3"><img src={item.avatar} width="40px" alt="John" />{item.name}</div>
            <div className="col-9 align-items-center" style={{color: item.colour}}>{item.message}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ChatBox;
