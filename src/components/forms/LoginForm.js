import React, { useState } from "react";
import { loginUser } from "../../api/backend";

const LoginForm = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [loginError, setLoginError] = useState("");

  const onLoginClick = async () => {
    setIsLoading(true);
    setLoginError("");
    let loginResult = null;

    try {
      loginResult = await loginUser(username, password);
      //console.log(loginResult);
    } catch (e) {
      setLoginError(e.message || e);
    } finally {
      setIsLoading(false);
      props.complete(loginResult);
    }
  };

  const onUsernameChange = (e) => setUsername(e.target.value.trim());
  const onPasswordChange = (e) => setPassword(e.target.value.trim());

  return (
    <form>
      <div>
        <label>Username</label>
        <input
          type="text"
          placeholder="Enter username..."
          onChange={onUsernameChange}
        />
      </div>
      <div>
        <label>Password</label>
        <input
          type="password"
          placeholder="Enter password..."
          onChange={onPasswordChange}
        />
      </div>
      <div>
        <button type="button" onClick={onLoginClick}>
          Submit
        </button>
      </div>
      {isLoading && <p>Logging in...</p>}
      {loginError && <p>{ loginError }</p>}
    </form>
  );
};

export default LoginForm;
