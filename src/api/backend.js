import { getStorage } from "../utils/localStorage";

const BACKEND_URL = "http://localhost:4000";

export const loginUser = (username, password) => {
  console.log(`Top of login user:\n username=${username}&password=${password}`);
  return fetch(
    `${BACKEND_URL}/user?username=${username}&password=${password}`,
    {
      method: "GET",
    }
  )
    .then((r) => {
      if(r.status < 400) {
        return r.json();
      } else {
        throw new Error("Couldn't login!");
      }
    })
    .catch((e) => {
      console.error(e)
      throw e;
    });
};

export const sendSettings = (settings) => {
  return fetch(
    `${BACKEND_URL}/user`,
    {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        token: getStorage('ra_session'),
        settings: settings
      })
    }
  )
    .then((r) => {
      if(r.status < 400) {
        return r.json();
      } else {
        throw new Error("Couldn't change settings!");
      }
    })
    .catch((e) => {
      console.error(e)
      throw e;
    });
};
