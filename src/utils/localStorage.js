export const setStorage = (key, value) => {
  // 1. Stringify Data
  const json = JSON.stringify(value);
  // 2. Encrypt
  const encrypted = btoa(json);
  // 3. Save in localStorage
  localStorage.setItem(key, encrypted);
};

export const getStorage = (key) => {
  // 1. Get from storage
  const storedValue = localStorage.getItem(key);
  if (!storedValue) return false;
  // 2. Decrypt
  const decrypted = atob(storedValue);
  // 3. Parse JSON and return
  return JSON.parse(decrypted);
};

export const parseJwt = (token) => {
  try {
    return JSON.parse(atob(token.split('.')[1]));
  } catch (e) {
    return null;
  }
};

export const getCurrentUser = () => {
  const unwrap = ({name, colour, avatar}) => ({name, colour, avatar});
  return unwrap(parseJwt(getStorage('ra_session')));
}