import React, { useState } from 'react';
import { sendSettings } from '../api/backend';
import { setStorage } from '../utils/localStorage';
import { useHistory } from 'react-router-dom';

const Settings = (props) => {
  const [user, setUser] = useState({...props.user});
  const [isLoading, setIsLoading] = useState(false);
  const [settingError, setSettingError] = useState("");

  const history = useHistory();

  const onChangeAvatar = (ev) => {
    let tempUser = user;
    tempUser.avatar = ev.target.value;
    setUser(tempUser);
  }

  const onChangeColour = (ev) => {
    let tempUser = user;
    tempUser.colour = ev.target.value;
    setUser(tempUser);
  }

  const onSubmitChanges = async () => {
    setIsLoading(true);
    setSettingError("");
    let settingsResult = null;

    try {
      settingsResult = await sendSettings(user);
      console.log(settingsResult);
      setStorage('ra_session', settingsResult);
      history.replace('/');
    } catch (e) {
      setSettingError(e.message || e);

    } finally {
      setIsLoading(false);
    }
  };
  return(
    <div>
      <h3>Settings for { user.name }</h3>
      <label htmlFor="avatar">Avatar url:</label>
      <input name="avatar" onChange={ onChangeAvatar } />
      <br />
      <label htmlFor="colour">Colour:</label>
      <input name="colour" onChange={ onChangeColour } />
      <br />
      <button type="button" onClick={onSubmitChanges}>Save changes</button>

      {isLoading && <p>Saving settings...</p>}
      {settingError && <p>{ settingError }</p>}
    </div>
  );
}

export default Settings;