import React from 'react'
import ChatBox from '../components/chat/ChatBox';
import ChatBar from '../components/chat/ChatBar';
import socketIOClient from "socket.io-client";
import ChatList from '../components/chat/ChatList';
import { getStorage, getCurrentUser } from '../utils/localStorage';
import { useHistory } from 'react-router-dom';

const ENDPOINT = "http://localhost:4000";
//const TEMP_JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBDZW5hIn0.rOeJ-EfTlDOOqmlzIOw25320bWdXg4gmo9FWC-zf7SI";

class Chat extends React.Component {

  constructor(props) {
    super(props);
    //const { token } = getStorage('ra_session');
    if(!getStorage('ra_session')) {
      console.log("Could not find ra_session");
      const history = useHistory();
      history.replace("/login");
    } else {
      this.state = {
        socket: socketIOClient(ENDPOINT),
        messages: [],
        users: []
      }
    }
  }

  sendMessage(message) {
    this.state.socket.emit('chat message', {
      message: message,
      ...getCurrentUser()
    });
  }

  chatMessage(message) {
    console.log("MESSAGE: "+ message.message + " from " + message.name);
    this.setState({
      messages: this.state.messages.concat(message)
    });
  }

  userConnected(user) {
    console.log(`User connected: ${user.name}`);
    let listUser = this.state.users.find((u) => u.name === user.name);
    if(!listUser) {
      this.setState({
        users: this.state.users.concat(user)
      });
    } else {
      console.log("Updating user "+ listUser.name);
      listUser = { ...user };
    }
  }

  userDisconnected(name) {
    console.log(`User disconnected: ${name}`);
    let usersList = this.state.users;
    for(let i = 0; i < usersList.length; i++) {
      if(usersList[i].name === name) {
        usersList.splice(i,1);
        this.setState({
          users: usersList
        });
        return;
      }
    }
  }

  componentDidMount() {
    const token = getStorage('ra_session');
    console.log("TOKEN::" + token);
    
    this.state.socket.on('error', error => console.error(error));
    this.state.socket.on('connect', () => {
      this.state.socket
        .emit('authenticate', { token: token }) //send the jwt
        .on('authenticated', () => {
          console.log("Authenticated");
          // --------- ADD AUTHENTICATED STUFF HERE -----------
          this.state.socket.on('chat message', message => this.chatMessage(message));
          this.state.socket.on('new user', userData => {
            this.userConnected(userData);
            console.log(`Sending a user update as ${getCurrentUser().name}`);
            this.state.socket.emit('user update', getCurrentUser()); // CHANGE THIS
          });
          this.state.socket.on('user update', userData => this.userConnected(userData));
          this.state.socket.on('disconnect', name => this.userDisconnected(name));
          
          // --------------------------------------------------
        })
        .on('unauthorized', () => {
          console.log(`unauthorized!`);
        })
    });
  }

  componentWillUnmount() {
    this.state.socket.removeAllListeners(); // Not sure if it's necessary
  }

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-8">
            <h1>Chat</h1>
            <ChatBox messages={ this.state.messages }/>
            <ChatBar sendMessage={ (message) => this.sendMessage(message) }/>
          </div>
          <div className="col-4">
            <ChatList users={ this.state.users } />
          </div>
        </div>
      </React.Fragment>
    )
  }
};

export default Chat;