import React from 'react';
import { useHistory } from 'react-router-dom';
import LoginForm from '../components/forms/LoginForm';
import { setStorage } from '../utils/localStorage';

const Login = (props) => {
  const history = useHistory();

  const onLoginComplete = (token) => {
    if(token !== null) {
      console.log("TOKEN from LoginForm:", token);
      setStorage("ra_session",  token);
      props.changeLoggedIn(true);
      history.replace('/');
    }
  };

  return (
    <div>
      <h1>Login</h1>
      <h3>First time logins creates a new user!</h3>
      <LoginForm complete={ onLoginComplete } />
    </div>
  )
}

export default Login;